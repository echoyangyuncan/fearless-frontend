
window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
        // Here, add the 'd-none' class to the loading icon
        // create a spinnerTag -> and adjust the class using classList.add
        // id of loading icone: "loading-conference-spinner"
        const spinnerTag = document.getElementById('loading-conference-spinner')
        spinnerTag.classList.add("d-none")

        // Here, remove the 'd-none' class from the select tag
        // for selectTag ->  adjust the class using classList.remove

        selectTag.classList.remove("d-none")
    }
    const formTag = document.getElementById("create-attendee-form") // add attendee form element by ID
    formTag.addEventListener('submit', async event => {
        event.preventDefault() 
    
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData)) // new object from form data entries
    
    
        const attendeeURL = "http://localhost:8001/api/attendees/"
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
    
        const attendeeResponse = await fetch(attendeeURL, fetchConfig)
        if (response.ok) {
            // remove d-none from success -> show success
            const successTag = document.getElementById('success-message')
            successTag.classList.remove("d-none")
    
            // add d-none to the form -> hide form
            formTag.classList.add("d-none")
    
        }
    
    })
  });